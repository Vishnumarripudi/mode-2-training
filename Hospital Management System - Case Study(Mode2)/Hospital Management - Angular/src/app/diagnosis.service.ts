import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
 
@Injectable({
  providedIn: 'root'
})
export class DiagnosisService {
 
  private baseUrl = 'http://localhost:8080/hospital/diagnosis';
  private url = 'http://localhost:8080/hospital/';
  constructor(private http: HttpClient) { }
 
  getPatientList():Observable<any>{
    return this.http.get(`${this.url}/list`);
  }
 
  
  
  
  generateDiagnosisReport(diagnosis:any):Observable<any>{
    return this.http.post(this.baseUrl,diagnosis);
  }
}