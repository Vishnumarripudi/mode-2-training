import { Component, OnInit } from '@angular/core';
import { Physician } from '../physician';
import { HospitalServiceService } from '../hospital-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-physician',
  templateUrl: './search-physician.component.html',
  styleUrls: ['./search-physician.component.css']
})
export class SearchPhysicianComponent implements OnInit {
  department: String;
  state: String;
  physicians: Physician[];
  constructor(private physicianService: HospitalServiceService, private router: Router) { }

  ngOnInit(): void {
    this.department="";
    this.state="";
  }
  private searchPhysicians() {
    this.physicians = [];
    this.physicianService.getPhysiciansByDepartment(this.department)
      .subscribe(physicians => this.physicians = physicians);
    this.physicianService.getPhysiciansByState(this.state)
      .subscribe(physicians => this.physicians = physicians);

      
  }

 
  onSubmit() {
    this.searchPhysicians();
    this.state=" ";
  }

  onSubmit1() {
    this.searchPhysicians();
    this.department=" ";
  }

  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }

}
