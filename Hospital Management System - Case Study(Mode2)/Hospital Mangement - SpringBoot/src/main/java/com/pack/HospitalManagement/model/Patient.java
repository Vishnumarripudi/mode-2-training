package com.pack.HospitalManagement.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "patient1")
public class Patient {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String firstName;

	private String LastName;
	private String password;

	private Date dateOfBirth;

	private String email;

	private long contactNumber;

	private String state;

	private String insurancePlan;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getInsurancePlan() {
		return insurancePlan;
	}

	public void setInsurancePlan(String insurancePlan) {
		this.insurancePlan = insurancePlan;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", firstName=" + firstName + ", LastName=" + LastName + ", password=" + password
				+ ", dateOfBirth=" + dateOfBirth + ", email=" + email + ", contactNumber=" + contactNumber + ", state="
				+ state + ", insurancePlan=" + insurancePlan + "]";
	}

	public Patient(long id, String firstName, String lastName, String password, Date dateOfBirth, String email,
			long contactNumber, String state, String insurancePlan) {
		super();
		this.id = id;
		this.firstName = firstName;
		LastName = lastName;
		this.password = password;
		this.dateOfBirth = dateOfBirth;
		this.email = email;
		this.contactNumber = contactNumber;
		this.state = state;
		this.insurancePlan = insurancePlan;
	}

	public Patient() {
		super();

	}

	public Patient(String firstName, String lastName, String password, Date dateOfBirth, String email,
			long contactNumber, String state, String insurancePlan) {
		super();
		this.firstName = firstName;
		LastName = lastName;
		this.password = password;
		this.dateOfBirth = dateOfBirth;
		this.email = email;
		this.contactNumber = contactNumber;
		this.state = state;
		this.insurancePlan = insurancePlan;
	}

}
