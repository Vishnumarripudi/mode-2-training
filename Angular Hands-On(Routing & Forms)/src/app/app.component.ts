import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ExcerciseProject';
  FavoriteMovie:string="Lord of the Rings";
  Date:Date = new Date('12/31/2000');
}
