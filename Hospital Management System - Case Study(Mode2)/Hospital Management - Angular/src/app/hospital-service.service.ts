import { Injectable } from '@angular/core';
import { Patient } from './patient';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Physician } from './physician';
const httpOptions = {
  headers: new HttpHeaders({ 
  'Access-Control-Allow-Origin':'*',
   
   })
  };
@Injectable({
  providedIn: 'root'
})
export class HospitalServiceService {
  
  

  constructor(private http : HttpClient) { }
  private url = 'http://localhost:8080/hospital';

  enrollPatient(patient: Patient): Observable<any> {

    return this.http.post(`${this.url}` + `/enroll`, patient);
    
  }

  addPhysician(physician: Physician): Observable<any>{

    return this.http.post(`${this.url}` + `/add`, physician);

  }

  searchPhysician(physician: Physician): Observable<any>{

    return this.http.post(`${this.url}` + `/search`, physician);

  }

  getPhysiciansByDepartment(department: String): Observable<any> {
   return this.http.get(`${this.url}/physicians/department/${department}`);
   
  }

  getPhysiciansByState(state: String): Observable<any> {
    return this.http.get(`${this.url}/physicians/state/${state}`);

  }

  
}
