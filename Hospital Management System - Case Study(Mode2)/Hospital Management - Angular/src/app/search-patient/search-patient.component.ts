import { Component, OnInit } from '@angular/core';
import { Patient } from '../patient';
import { HospitalServiceService } from '../hospital-service.service';
import { PatienthistoryService } from '../patienthistory.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-patient',
  templateUrl: './search-patient.component.html',
  styleUrls: ['./search-patient.component.css']
})

export class SearchPatientComponent implements OnInit {​​​​​
firstName: string;
id: number;
patients: Patient[];
constructor(private patientService: PatienthistoryService, private router: Router) {​​​​​ }​​​​​
ngOnInit(): void {​​​​​
  }​​​​​
private searchPatients() {​​​​​
this.patients = [];
this.patientService.getPatientsByFirstName(this.firstName)
      .subscribe(patients=>this.patients = patients);

  }​​​​​
onSubmit() {​​​​​
this.searchPatients();
  }​​​​​

  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }
}​​​​​
