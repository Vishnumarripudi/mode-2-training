export class Patient{
    id : number;
    firstName : string;
    lastName : string;
    password : string;
    dateOfBirth : Date;
    email : string;
    contactNumber : number;
    state : string;
    insurancePlan : string;
}