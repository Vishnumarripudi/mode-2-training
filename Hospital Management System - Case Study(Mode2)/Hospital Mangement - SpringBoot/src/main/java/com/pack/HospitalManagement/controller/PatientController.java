package com.pack.HospitalManagement.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pack.HospitalManagement.dao.PatientDiagnosisRepository;
import com.pack.HospitalManagement.dao.PatientRepository;
import com.pack.HospitalManagement.dao.PhysicianRepository;
import com.pack.HospitalManagement.model.Patient;
import com.pack.HospitalManagement.model.PatientDiagnosis;
import com.pack.HospitalManagement.model.Physician;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/hospital")
public class PatientController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);
	
	@Autowired
	PatientRepository repository;

	@PostMapping(value = "/enroll")
	public ResponseEntity<Patient> savePatient(@RequestBody Patient patient) {
		try {
			Patient patient1 = repository.save(new Patient(patient.getFirstName(), patient.getLastName(),
					patient.getPassword(), patient.getDateOfBirth(), patient.getEmail(), patient.getContactNumber(),
					patient.getState(), patient.getInsurancePlan()));
			
			LOGGER.info("Patient Details Added");


			return new ResponseEntity<>(patient1, HttpStatus.CREATED);
		} catch (Exception e) {
		
			LOGGER.info("Exception in Save Patient");


			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "/list")
	public ResponseEntity<List<Patient>> getAllPatients() {
		List<Patient> patients = new ArrayList<Patient>();
		try {
			repository.findAll().forEach(patients::add);
			if (patients.isEmpty()) {
				
				LOGGER.info("Empty Patient Details");
				
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			 LOGGER.info("Patient Details Found");
			return new ResponseEntity<>(patients, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.info("Exception in Get All Patients");
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Autowired
	PhysicianRepository repository1;

	@PostMapping(value = "/add")
	public ResponseEntity<Physician> savePhysician(@RequestBody Physician physician) {
		try {
			Physician physician1 = repository1.save(new Physician(physician.getFirstName(), physician.getLastName(),
					physician.getDepartment(), physician.getEducationalQualification(),
					physician.getYearsOfExperience(), physician.getState(), physician.getInsurancePlan()));
			LOGGER.info("Physician Details Added");
			return new ResponseEntity<>(physician1, HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.info("Exception in add Physician");
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "/physicians/department/{department}")
	public ResponseEntity<List<Physician>> findByDepartment(@PathVariable String department) {
		try {
			List<Physician> physicians = repository1.findByDepartment(department);

			if (physicians.isEmpty()) {
				
				LOGGER.info("Empty Physicians Details");
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			 LOGGER.info("Physicians Details Displayed");
			return new ResponseEntity<>(physicians, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.info("Exception in Get All Physicians");
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "/physicians/state/{state}")
	public ResponseEntity<List<Physician>> findByState(@PathVariable String state) {
		try {
			List<Physician> physicians = repository1.findByState(state);

			if (physicians.isEmpty()) {
				LOGGER.info("Empty Physicians Details");
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			LOGGER.info("Physicians Details Displayed");
			return new ResponseEntity<>(physicians, HttpStatus.OK);
		} catch (Exception e) {
			
			LOGGER.info("Exception in Get All Physicians");
			
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@Autowired
	PatientDiagnosisRepository repository2;

	@PostMapping(value = "/diagnosis")
	public ResponseEntity<PatientDiagnosis> savePatientDiagnosis(@RequestBody PatientDiagnosis patientDiagnosis) {
		try {
			PatientDiagnosis patientDiagnosis1 = repository2
					.save(new PatientDiagnosis(patientDiagnosis.getPatientId(), patientDiagnosis.getSymptoms(),
							patientDiagnosis.getDiagnosisProvided(), patientDiagnosis.getAdministeredBy(),
							patientDiagnosis.getDateOfDiagnosis(), patientDiagnosis.getFollowUpRequired(),
							patientDiagnosis.getDateOfFollowUp(), patientDiagnosis.getBillAmount(),
							patientDiagnosis.getCardNumber(), patientDiagnosis.getModeOfPayment()));
			LOGGER.info("Patient Diagnosis Details Added");
			return new ResponseEntity<>(patientDiagnosis1, HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.info("Exception in adding patient diagnosis details");
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@Autowired
	PatientRepository patientRepository;

	@GetMapping(value = "patients/firstName/{firstName}")
	public ResponseEntity<List<Patient>> findByFirstName(@PathVariable String firstName) {
		try {
			List<Patient> patient = patientRepository.findByFirstName(firstName);

			if (patient.isEmpty()) {
				LOGGER.info("Empty Patient Details");
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			 LOGGER.info("Patient Details Found");
			return new ResponseEntity<>(patient, HttpStatus.OK);
		} catch (Exception e) {
			
			LOGGER.info("Exception in Get All Patiens");
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

}
