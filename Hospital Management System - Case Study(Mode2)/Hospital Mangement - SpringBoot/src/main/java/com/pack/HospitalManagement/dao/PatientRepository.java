package com.pack.HospitalManagement.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pack.HospitalManagement.model.Patient;

public interface PatientRepository extends CrudRepository<Patient, Long> {

	List<Patient> findByFirstName(String firstName);

}
