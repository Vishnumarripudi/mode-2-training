package com.pack.HospitalManagement.dao;

import org.springframework.data.repository.CrudRepository;

import com.pack.HospitalManagement.model.PatientDiagnosis;

public interface PatientDiagnosisRepository extends CrudRepository<PatientDiagnosis, Long> {

}
