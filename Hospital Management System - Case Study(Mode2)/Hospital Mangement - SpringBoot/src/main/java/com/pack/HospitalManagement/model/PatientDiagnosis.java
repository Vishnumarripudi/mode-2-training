package com.pack.HospitalManagement.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "patientdiagnosis1")
public class PatientDiagnosis {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private long patientId;

	private String symptoms;

	private String diagnosisProvided;

	private String administeredBy;

	private Date dateOfDiagnosis;

	private String followUpRequired;

	private Date dateOfFollowUp;

	private long billAmount;

	private long cardNumber;

	private String modeOfPayment;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getDiagnosisProvided() {
		return diagnosisProvided;
	}

	public void setDiagnosisProvided(String diagnosisProvided) {
		this.diagnosisProvided = diagnosisProvided;
	}

	public String getAdministeredBy() {
		return administeredBy;
	}

	public void setAdministeredBy(String administeredBy) {
		this.administeredBy = administeredBy;
	}

	public Date getDateOfDiagnosis() {
		return dateOfDiagnosis;
	}

	public void setDateOfDiagnosis(Date dateOfDiagnosis) {
		this.dateOfDiagnosis = dateOfDiagnosis;
	}

	public String getFollowUpRequired() {
		return followUpRequired;
	}

	public void setFollowUpRequired(String followUpRequired) {
		this.followUpRequired = followUpRequired;
	}

	public Date getDateOfFollowUp() {
		return dateOfFollowUp;
	}

	public void setDateOfFollowUp(Date dateOfFollowUp) {
		this.dateOfFollowUp = dateOfFollowUp;
	}

	public long getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(long billAmount) {
		this.billAmount = billAmount;
	}

	public long getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	@Override
	public String toString() {
		return "PatientDiagnosis [id=" + id + ", patientId=" + patientId + ", symptoms=" + symptoms
				+ ", diagnosisProvided=" + diagnosisProvided + ", administeredBy=" + administeredBy
				+ ", dateOfDiagnosis=" + dateOfDiagnosis + ", followUpRequired=" + followUpRequired
				+ ", dateOfFollowUp=" + dateOfFollowUp + ", billAmount=" + billAmount + ", cardNumber=" + cardNumber
				+ ", modeOfPayment=" + modeOfPayment + "]";
	}

	public PatientDiagnosis(long id, long patientId, String symptoms, String diagnosisProvided, String administeredBy,
			Date dateOfDiagnosis, String followUpRequired, Date dateOfFollowUp, long billAmount, long cardNumber,
			String modeOfPayment) {
		super();
		this.id = id;
		this.patientId = patientId;
		this.symptoms = symptoms;
		this.diagnosisProvided = diagnosisProvided;
		this.administeredBy = administeredBy;
		this.dateOfDiagnosis = dateOfDiagnosis;
		this.followUpRequired = followUpRequired;
		this.dateOfFollowUp = dateOfFollowUp;
		this.billAmount = billAmount;
		this.cardNumber = cardNumber;
		this.modeOfPayment = modeOfPayment;
	}

	public PatientDiagnosis(long patientId, String symptoms, String diagnosisProvided, String administeredBy,
			Date dateOfDiagnosis, String followUpRequired, Date dateOfFollowUp, long billAmount, long cardNumber,
			String modeOfPayment) {
		super();
		this.patientId = patientId;
		this.symptoms = symptoms;
		this.diagnosisProvided = diagnosisProvided;
		this.administeredBy = administeredBy;
		this.dateOfDiagnosis = dateOfDiagnosis;
		this.followUpRequired = followUpRequired;
		this.dateOfFollowUp = dateOfFollowUp;
		this.billAmount = billAmount;
		this.cardNumber = cardNumber;
		this.modeOfPayment = modeOfPayment;
	}

	public PatientDiagnosis() {
		super();

	}

}
