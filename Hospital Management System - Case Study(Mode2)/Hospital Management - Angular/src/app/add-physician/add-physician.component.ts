import { Component, OnInit } from '@angular/core';
import { Physician } from '../physician';
import { HospitalServiceService } from '../hospital-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-physician',
  templateUrl: './add-physician.component.html',
  styleUrls: ['./add-physician.component.css']
})
export class AddPhysicianComponent implements OnInit {
  physician : Physician = new Physician();
  submitted = false;
  constructor(private physicianService : HospitalServiceService, private router:Router) { }

  ngOnInit(): void {
  }
  newPhysician():void{ 
    this.submitted=false;
    this.physician=new Physician();
  }  
  save(){ 
    console.log(this.physician.state);
    this.physicianService.addPhysician(this.physician).
    subscribe(
      data=>{ 
        console.log(data);
        this.submitted=true; 
      },
      error=>console.log(error));
      this.physician=new Physician();
  }    
  onSubmit(){
    this.save();
  }

  resetForm(){
    this.physician= new Physician();
  }

  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }
}