import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EntrollPatientComponent } from './entroll-patient/entroll-patient.component';
import { HttpClientModule } from '@angular/common/http';
import { AddPhysicianComponent } from './add-physician/add-physician.component';
import { SearchPhysicianComponent } from './search-physician/search-physician.component';
import { PatientDiagnosisdetailsComponent } from './patient-diagnosisdetails/patient-diagnosisdetails.component';
import { SearchPatientComponent } from './search-patient/search-patient.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { ItemsComponent } from './items/items.component';


@NgModule({
  declarations: [
    AppComponent,
    EntrollPatientComponent,
    AddPhysicianComponent,
    SearchPhysicianComponent,
    PatientDiagnosisdetailsComponent,
    SearchPatientComponent,
    AdminComponent,
    LoginComponent,
    ItemsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }










