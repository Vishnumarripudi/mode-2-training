
export class PatientDiagnosis{
    id:number;
    patientId:number;
    symptoms:String;
    diagnosisProvided:String;
    administeredBy:String;
    dateOfDiagnosis:Date;
    followUpRequired:String;
    dateOfFollowUp:Date;
    billAmount:number;
    cardNumber:number;
    modeOfPayment:String;
}