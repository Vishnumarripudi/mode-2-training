import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntrollPatientComponent } from './entroll-patient.component';

describe('EntrollPatientComponent', () => {
  let component: EntrollPatientComponent;
  let fixture: ComponentFixture<EntrollPatientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EntrollPatientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntrollPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
