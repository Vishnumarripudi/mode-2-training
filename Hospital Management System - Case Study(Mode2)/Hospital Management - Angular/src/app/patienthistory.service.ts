import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PatienthistoryService {

  constructor(private http : HttpClient) { }
  private url = 'http://localhost:8080/hospital/patients';

  getPatientsByFirstName(firstName: string): Observable<any> {
    return this.http.get(`${this.url}/firstName/${firstName}`);
  }
}
