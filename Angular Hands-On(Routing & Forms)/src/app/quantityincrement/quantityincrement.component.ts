import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quantityincrement',
  templateUrl: './quantityincrement.component.html',
  styleUrls: ['./quantityincrement.component.css']
})
export class QuantityIncrementComponent implements OnInit {
 
  public buttonClicked = false
   
    constructor(private router: Router) { }
   
    ngOnInit(): void {
    }
   
    onClick(){
      this.router.navigate(['/quantityincrement']),
      this.buttonClicked=true;
      
  }
  }