package com.pack.HospitalManagement.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "physician1")
public class Physician {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String firstName;

	private String LastName;
	private String department;

	private String educationalQualification;

	private long yearsOfExperience;

	private String state;

	private String insurancePlan;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEducationalQualification() {
		return educationalQualification;
	}

	public void setEducationalQualification(String educationalQualification) {
		this.educationalQualification = educationalQualification;
	}

	public long getYearsOfExperience() {
		return yearsOfExperience;
	}

	public void setYearsOfExperience(long yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getInsurancePlan() {
		return insurancePlan;
	}

	public void setInsurancePlan(String insurancePlan) {
		this.insurancePlan = insurancePlan;
	}

	@Override
	public String toString() {
		return "Physician [id=" + id + ", firstName=" + firstName + ", LastName=" + LastName + ", department="
				+ department + ", educationalQualification=" + educationalQualification + ", yearsOfExperience="
				+ yearsOfExperience + ", state=" + state + ", insurancePlan=" + insurancePlan + "]";
	}

	public Physician(long id, String firstName, String lastName, String department, String educationalQualification,
			long yearsOfExperience, String state, String insurancePlan) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.LastName = lastName;
		this.department = department;
		this.educationalQualification = educationalQualification;
		this.yearsOfExperience = yearsOfExperience;
		this.state = state;
		this.insurancePlan = insurancePlan;
	}

	public Physician(String firstName, String lastName, String department, String educationalQualification,
			long yearsOfExperience, String state, String insurancePlan) {
		super();
		this.firstName = firstName;
		this.LastName = lastName;
		this.department = department;
		this.educationalQualification = educationalQualification;
		this.yearsOfExperience = yearsOfExperience;
		this.state = state;
		this.insurancePlan = insurancePlan;
	}

	public Physician() {
		super();
		// TODO Auto-generated constructor stub
	}

}
