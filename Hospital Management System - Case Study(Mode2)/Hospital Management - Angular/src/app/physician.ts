export class Physician{
    id : number;
    firstName : string;
    lastName : string;
    department : string;
    educationalQualification : string;
    yearsOfExperience : number;
    state : string;
    insurancePlan : string;
}