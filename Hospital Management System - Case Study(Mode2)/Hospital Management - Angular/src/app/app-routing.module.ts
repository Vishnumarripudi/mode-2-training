import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EntrollPatientComponent } from './entroll-patient/entroll-patient.component';
import { AddPhysicianComponent } from './add-physician/add-physician.component';
import { SearchPhysicianComponent } from './search-physician/search-physician.component';
import { PatientDiagnosisdetailsComponent } from './patient-diagnosisdetails/patient-diagnosisdetails.component';
import { SearchPatientComponent } from './search-patient/search-patient.component';
import { ItemsComponent } from './items/items.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './auth-guard.service';

const routes: Routes = [
  { path: 'ItemsAdmin/enrollPatient', component: EntrollPatientComponent },
  { path: 'ItemsAdmin/addPhysician', component: AddPhysicianComponent},
  { path: 'ItemsAdmin/searchPhysician', component: SearchPhysicianComponent},
  { path: 'ItemsAdmin/patientDiagnosisDetails', component: PatientDiagnosisdetailsComponent},
  { path: 'ItemsAdmin/viewPatientHistory', component: SearchPatientComponent},
  {
    path:'item',
    component:ItemsComponent
  },
  {
    path:'ItemsAdmin',
    component:AdminComponent,
    canActivate: [AuthGuardService]
},
{
  path:'Login',
  component:LoginComponent,
  
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


