import { Component, OnInit } from '@angular/core';
import { PatientDiagnosis } from '../patientDiagnosis';

import { Observable } from 'rxjs';
import { Physician } from '../Physician';
import { Patient } from '../Patient';
import { DiagnosisService } from '../diagnosis.service';
import { HospitalServiceService } from '../hospital-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient-diagnosisdetails',
  templateUrl: './patient-diagnosisdetails.component.html',
  styleUrls: ['./patient-diagnosisdetails.component.css']
})
export class PatientDiagnosisdetailsComponent implements OnInit {

  constructor(private diagnosisService:DiagnosisService,private patientService:DiagnosisService, private router: Router) { }
  d:number;
  dummy:Observable<Patient>;
  ngOnInit(): void {
    this.reloadData();
  }
  diagnosis:PatientDiagnosis=new PatientDiagnosis(); 
  submitted=false;
 
  newReport():void{
    this.submitted=false;
    this.diagnosis=new PatientDiagnosis();
  }
  save(){
    console.log(this.diagnosis);
    console.log(this.diagnosis.patientId);
    this.diagnosisService.generateDiagnosisReport(this.diagnosis).
    subscribe(
      data=>{
        console.log(data);
        
        this.submitted=true;
      },
      error=>console.log(error));
      this.diagnosis=new PatientDiagnosis();
  }
  onSubmit(){
    
 
    this.save(); 
  } 
 
  patients:Observable<Patient[]>;
  physicians:Observable<Physician[]>;
  reloadData(){
    this.patients=this.patientService.getPatientList();
    console.log(this.patients);
  } 

  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }
 
}