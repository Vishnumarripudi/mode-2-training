package com.pack.HospitalManagement.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pack.HospitalManagement.model.Physician;

public interface PhysicianRepository extends CrudRepository<Physician, Long> {

	List<Physician> findByDepartment(String department);

	List<Physician> findByState(String state);

}
