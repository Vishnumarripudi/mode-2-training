import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditemployeeComponent } from './editemployee/editemployee.component';
import { QuantityIncrementComponent } from './quantityincrement/quantityincrement.component';
import { ViewemployeeComponent } from './viewemployee/viewemployee.component';
import { EditEmpTemplateDrivenComponent } from './edit-emp-template-driven/edit-emp-template-driven.component';

 
const routes: Routes = [
  {path:'viewemployee', component:ViewemployeeComponent},
  {path:'editemployee',component:EditemployeeComponent},
  {path:'quantityincrement',component:QuantityIncrementComponent},
  {path:'edit-emp-template-driven',component:EditEmpTemplateDrivenComponent}
];
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }