import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientDiagnosisdetailsComponent } from './patient-diagnosisdetails.component';

describe('PatientDiagnosisdetailsComponent', () => {
  let component: PatientDiagnosisdetailsComponent;
  let fixture: ComponentFixture<PatientDiagnosisdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientDiagnosisdetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDiagnosisdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
