import { Component, OnInit } from '@angular/core';
import { Patient } from '../patient';
import { HospitalServiceService } from '../hospital-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-entroll-patient',
  templateUrl: './entroll-patient.component.html',
  styleUrls: ['./entroll-patient.component.css']
})
export class EntrollPatientComponent implements OnInit {

  patient : Patient = new Patient();
  submitted = false;
  constructor(private patientService : HospitalServiceService, private router:Router) { }

  ngOnInit(): void {
  }

  newPatient():void{ 
    this.submitted=false;
    this.patient=new Patient();
  }  
  save(){ 
    console.log(this.patient.state);
    this.patientService.enrollPatient(this.patient).
    subscribe(
      data=>{ 
        console.log(data);
        this.submitted=true; 
      },
      error=>console.log(error));
      this.patient=new Patient();
  }    
  onSubmit(){
    this.save();
  }
  btnClick(){
    this.router.navigateByUrl('ItemsAdmin');
  }


  
}