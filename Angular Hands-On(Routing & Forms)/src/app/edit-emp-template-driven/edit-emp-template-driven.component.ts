import { Component, OnInit } from '@angular/core';
import { Employee } from '../Employee';
import { Router } from '@angular/router';
import { Department } from '../Department';
 
@Component({
  selector: 'app-edit-emp-template-driven',
  templateUrl: './edit-emp-template-driven.component.html',
  styleUrls: ['./edit-emp-template-driven.component.css']
})
export class EditEmpTemplateDrivenComponent implements OnInit {
  emp: Employee = {
    id: 10,
    name: 'Jhon',
    date: '12/31/2000',
    gender: 'male',
    salary: 90000,
    permanent:'true',
    department: {departmentId:1,departmentName:"PayRoll"},
    skill: [
      { skill_id: 1, skill_name: 'HTML' },
      { skill_id: 2, skill_name: 'CSS' },
      { skill_id: 3, skill_name: 'JAVASCRIPT' },
    ],
    dateOfBirth: new Date('12/31/2000')
  };
  constructor(private route:Router) { }
 
  ngOnInit(): void {
  }
  dept:Department[] = [
    { departmentId: 1, departmentName: "Payroll" },   
    { departmentId: 2, departmentName: "Internal" },
    { departmentId: 3, departmentName: "HR" }
  ];
  
  submitted = false;
  onSubmit() {
    console.log("clicked");
    this.submitted = true;
    if(this.submitted==true){
    this.route.navigate(['']);
    }
  }
} 