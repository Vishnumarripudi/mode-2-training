import { Component, OnInit } from '@angular/core';
import { Employee } from '../Employee';
import { Department } from '../Department';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  emp: Employee = {
    id: 10,
    name: 'Vishnu',
    date: '10/10/1998',
    gender: 'male',
    salary: 90000,
    permanent:'true',
    department: {departmentId:1000,departmentName:"PayRoll"},
    skill: [
      { skill_id: 1, skill_name: 'HTML' },
      { skill_id: 2, skill_name: 'CSS' },
      { skill_id: 3, skill_name: 'JAVASCRIPT' },
    ],
    dateOfBirth: new Date('12/31/2000')
  };
  
  constructor() { }

  ngOnInit(): void {
  }

}
